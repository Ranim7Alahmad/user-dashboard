<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Corona Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('vendors/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel-2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel-2/owl.theme.default.min.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_sidebar.html -->

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar p-0 fixed-top d-flex flex-row">
            <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="mdi mdi-menu"></span>
                </button>
                <ul class="navbar-nav w-100">
                    <li class="nav-item w-100">
                        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
                            <input type="text" class="form-control" placeholder="Search task user">
                        </form>
                    </li>
                </ul>

                <ul class="navbar-nav navbar-nav-right">
                    @if(auth()->user()->is_Admin)
                        <li class="nav-item dropdown d-none d-lg-block">
                          <a class="nav-link btn btn-success create-new-button" href="/getUser">Users </a>
                        </li>
                        <li class="nav-item dropdown d-none d-lg-block">
                          <a class="nav-link btn btn-success create-new-button" href="/home">Tasks </a>
                        </li>

                    <li class="nav-item dropdown d-none d-lg-block">
                        <a class="nav-link btn btn-success create-new-button" data-toggle="modal" data-target="#exampleModal" >+ Create New Task</a>
                    </li>
                    @endif

                    <li class="nav-item dropdown">
                        <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
                            <div class="navbar-profile">
                                <p class="mb-0 d-none d-sm-block navbar-profile-name">Admin</p>
                                <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                            <h6 class="p-3 mb-0">Profile</h6>
                            <div class="dropdown-divider"></div>
                            @if(auth()->user()->is_Admin)
                            <a  href="/register"  class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-dark rounded-circle">
                                        <i class="mdi mdi-login text-success"></i>
                                    </div>
                                </div>

                                <div class="preview-item-content">
                                    <p class="preview-subject mb-1">Add user</p>
                                </div>
                            </a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a  href="/logout" class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-dark rounded-circle">
                                        <i class="mdi mdi-logout text-danger"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">

                                        <p class="preview-subject mb-1">Log out</p>

                                </div>
                            </a>

                        </div>
                    </li>
                </ul>

                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-format-line-spacing"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                <div class="row ">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            @if($getUsertrue!=1)
                            <div class="card-body">
                                <div class="row ">
                                    <h4 class="card-title col-8">Tasks</h4>

                                    <form id="getTa" action="" method="get">
                                        @if(auth()->user()->is_Admin)
                                            <select  class="form-select  mr-3"style="height: 30px" style="width: 100% " aria-label="Default select example" style="padding: 6px"  id="user_id" name="user_id">
                                                <option value="0" >   </option>
                                            @foreach($userinf as $userin)
                                                    <option value="{{$userin->id}}" >{{$userin->name}}</option>
                                                @endforeach

                                            </select>
                                        @endif

                                        <input class="form-group last mb-4 mr-3 input--style-4 js-datepicker" id="dateT" type="date" name="dateT">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>


                                        <input  type="submit" style="background-color: transparent"  value="💚" class="btn btn-primary getTask ">
                                    </form>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                           @if(auth()->user()->is_Admin &&$getUsertrue!=2)
                                            <th> User Name </th>
                                              <th> Email </th>
                                            @endif
                                            <th> Task Name </th>
                                            <th> description </th>
                                            <th> Status </th>
                                            <th> </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if(auth()->user()->is_Admin &&$getUsertrue!=2)
                                        @foreach($tasks as $task)
                                         @foreach($task->task as $t)
                                        <tr data-task-id="{{$t->id}}" data-username="{{$task->name}}"  data-desc="{{$t->description}}" data-taskname="{{$t->name}}" data-status="{{$t->status}}">
                                                <td>
                                                    <span class="pl-2">{{$task->name}}</span>
                                                </td>
                                            <td> {{$task->email}}</td>
                                            <td>{{$t->name}}</td>
                                            <td>{{$t->description}}</td>
                                            @if($t->status == "pending")

                                            <td><div class="badge badge-outline-danger">{{$t->status}}</div></td>

                                            @elseif($t->status == "inprogress")

                                            <td><div class="badge badge-outline-warning">{{$t->status}}</div></td>

                                            @elseif($t->status == "completed")

                                            <td><div class="badge badge-outline-success">{{$t->status}}</div></td>
                                            @endif
                                            <td>
                                                <div class="row">

                                                        <button class="editTask badge badge-outline-success"  data-toggle="modal" data-target="#exampleModal2" id="createbuttonDropdown" style="background-color: transparent">Edit</button>


                                                <form  action="{{url('/task/deletetask/'.$t->id)}}" method="get">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button  type="hidden" name="method" value="DELETE" style="background-color: transparent" class="badge badge-outline-danger">Delete</button>
                                               </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                        @else
                                            @foreach($tasks as $t)
                                                <tr data-task-id2="{{$t->id}}"  data-desc2="{{$t->description}}" data-taskname2="{{$t->name}}" data-status2="{{$t->status}}">
                                                        <td>{{$t->name}}</td>
                                                        <td>{{$t->description}}</td>
                                                        @if($t->status == "pending")

                                                            <td><div class="badge badge-outline-danger">{{$t->status}}</div></td>

                                                        @elseif($t->status == "inprogress")

                                                            <td><div class="badge badge-outline-warning">{{$t->status}}</div></td>

                                                        @elseif($t->status == "completed")

                                                            <td><div class="badge badge-outline-success">{{$t->status}}</div></td>
                                                        @endif
                                                        <td>
                                                            <div class="row">
                                                            <button   class="editTask2 badge badge-outline-success" data-toggle="modal" data-target="#exampleModal2" id="createbuttonDropdown" style="background-color: transparent">Edit</button>
                                                                @if(auth()->user()->is_Admin)
                                                                    <form  action="{{url('/task/deletetask/'.$t->id)}}" method="get">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <button  type="hidden" name="method" value="DELETE" style="background-color: transparent" class="badge badge-outline-danger">Delete</button>
                                                                    </form>
                                                                 @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            @else
                                <div class="card-body">
                                    <h4 class="card-title">Users</h4>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th> User Name </th>
                                                <th> Email </th>
                                                <th> birthday </th>
                                                <th> gender </th>
                                                <th> </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($userinf as $user)

                                                        <tr data-user-id="{{$user->id}}" data-username1="{{$user->name}}"data-Password="{{$user->Password}}"  data-em="{{$user->email}}" data-birthday="{{$user->birthday}}" data-gender="{{$user->gender}}">
                                                            <td>
                                                                <span class="pl-2">{{$user->name}}</span>
                                                            </td>
                                                            <td> {{$user->email}}</td>
                                                            <td>{{$user->birthday}}</td>
                                                            @if($user->gender == "male")

                                                                <td><div style="color: #0d95e8">{{$user->gender}}</div></td>

                                                            @elseif($user->gender == "female")

                                                                <td><div style="color:deeppink">{{$user->gender}}</div></td>
                                                            @endif
                                                            <td>
                                                                <div class="row">

                                                                    <button class="editTask3 badge badge-outline-success"  data-toggle="modal" data-target="#exampleModal3" id="createbuttonDropdown" style="background-color: transparent">Edit</button>


                                                                    @if($user->is_Admin==0)
                                                                    <form  action="{{url('/user/delete/'.$user->id)}}" method="get">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <button  type="hidden" name="method" value="DELETE" style="background-color: transparent" class="badge badge-outline-danger">Delete</button>
                                                                   @endif

                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" >
                            <div class="container">
                                <div class="row">

                                    <div class="col-md-12 contents">
                                        <div class="row justify-content-center">
                                            <div class="col-md-8">
                                                <form action="{{route('CreateTask')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="form-group first mt-4" style="padding: 6px">

                                                        <label for="user_id">User Name</label>
                                                        <select type="submit" class="form-select " style="width: 100%" aria-label="Default select example" style="padding: 6px" name="user_id">
                                                            @foreach($userinf as $userin)
                                                                <option value="{{$userin->id}}" >{{$userin->name}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    <div class="form-group first mt-4" style="padding: 6px">
                                                        <label for="name">Task Name</label>
                                                        <input type="text" name="name" class="form-control">
                                                    </div>
                                                    <div class="form-group first mt-4" style="padding: 6px">
                                                        <label for="description">Description</label>
                                                        <input type="text" name="description" class="form-control">

                                                    </div>
                                                    <label for="user_id">Status</label>
                                                    <select class="form-select "style="width: 100%"  aria-label="Default select example" style="padding:6px  " name="status" >

                                                        <option value="pending">pending</option>
                                                        <option value="inprogress">inprogress</option>
                                                        <option value="completed">completed</option>
                                                    </select>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input  type="submit" value="Submit" class="btn btn-primary">
                                                    </div>

                                                </form>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel2">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" >
                            <div class="container">
                                <div class="row gallery">

                                    <div class="col-md-12 contents">
                                        <div class="row justify-content-center">
                                            <div class="col-md-8">
                                                <form id="editTaskForm"  action="" method="put">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" id="taskId" name="taskId">



                                                    <div class="form-group first mt-4" style="padding: 6px">
                                                        <label for="name">Task Name</label>
                                                        <input type="text" id="taskname" name="name" class="form-control">
                                                    </div>
                                                    <div class="form-group first mt-4" style="padding: 6px">
                                                        <label for="description">Description</label>
                                                        <input type="text" id="description" name="description" class="form-control">

                                                    </div>
                                                    <label for="user_id">Status</label>
                                                    <select class="form-select "style="width: 100%"  aria-label="Default select example" style="padding:6px  " name="status" id="status">

                                                        <option value="pending">pending</option>
                                                        <option value="inprogress">inprogress</option>
                                                        <option value="completed">completed</option>
                                                    </select>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input  type="submit"  value="Submit" class="btn btn-primary  ">
                                                    </div>

                                                </form>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel3">Edit User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" >
                            <div class="container">
                                <div class="row gallery">

                                    <div class="col-md-12 contents">
                                        <div class="row justify-content-center">
                                            <div class="col-md-8">
                                                <form id="editUserForm"  action="" method="put">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" id="userid" name="">
                                                        <div class="form-group first mt-4" style="padding: 6px">
                                                            <label for="name">Name</label>
                                                            <input id="name" type="text" name="name" class="form-control">

                                                        </div>
                                                        <div class="form-group first mt-4" style="padding: 6px">
                                                            <label for="email">Email</label>
                                                            <input id="email" type="email" name="email" class="form-control">

                                                        </div>

                                                        <label for="birthday">Birthday</label>
                                                        <input class="form-group last mb-4 mt-4 input--style-4 js-datepicker" type="date" id="birthday" name="birthday">
                                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>

                                                        <div class="form-group  mb-4 mt-4" style="padding: 6px">
                                                            <label for="password">Password</label>
                                                            <input type="password" aria-invalid="Password" name="password" class="form-control" id="">

                                                        </div>
                                                        <div class="form-group last  mb-4 mt-4" style="padding: 6px">
                                                            <label for="confirm_password">confirm_password</label>
                                                            <input type="password"id="confirm_password" name="confirm_password" class="form-control">

                                                        </div>

                                                        <select name="gender" id="gender">
                                                            <option value="male">male</option>
                                                            <option value="female">female</option>
                                                        </select>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input  type="submit"  value="Edit" class="btn btn-primary  ">
                                                    </div>

                                                    </form>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->

<!-- container-scroller -->
<!-- plugins:js -->
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('vendors/progressbar.js/progressbar.min.js')}}"></script>
<script src="{{asset('vendors/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('vendors/owl-carousel-2/owl.carousel.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('js/off-canvas.js')}}"></script>
<script src="{{asset('js/hoverable-collapse.js')}}"></script>
<script src="{{asset('js/misc.js')}}"></script>
<script src="{{asset('js/settings.js')}}"></script>
<script src="{{asset('js/todolist.js')}}"></script>
<script src="{{asset('js/dashboard.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script>
    $(document).ready(function(){
        $(".editTask").on("click", (event)=>{
            var username =  $(event.target).closest('tr').attr("data-username");
            var taskname =  $(event.target).closest('tr').attr("data-taskname");
            var datataskid =  $(event.target).closest('tr').attr("data-task-id");
            var datadesc =  $(event.target).closest('tr').attr("data-desc");
            var datastatus =  $(event.target).closest('tr').attr("data-status");
                   $('#user_id').val(username);
                   $('#taskname').val(taskname);
                   $('#status').val(datastatus);
                   $('#description').val(datadesc);
                   $('#taskid').val(datataskid);

            var dataId = $(this).attr("data-username");
            document.getElementById('editTaskForm').action = "task/edit/"+datataskid;
            console.log(document.getElementById('editTaskForm').action);
            console.log($('formEdit').attr('action'));

        });


    });
</script>
<script>
    $(document).ready(function(){
        $(".editTask2").on("click", (event)=>{
            var taskname2 =  $(event.target).closest('tr').attr("data-taskname2");
            var datataskid2 =  $(event.target).closest('tr').attr("data-task-id2");
            var datadesc2 =  $(event.target).closest('tr').attr("data-desc2");
            var datastatus2 =  $(event.target).closest('tr').attr("data-status2");
                   $('#taskname').val(taskname2);
                   $('#status').val(datastatus2);
                   $('#description').val(datadesc2);
                   $('#taskidtd').val(datataskid2);
            var dataId = $(this).attr("datataskid2");
            document.getElementById('editTaskForm').action = "/task/edit/"+datataskid2;
            console.log(document.getElementById('editTaskForm').action);

        });


    });
</script>
<script>
    $(document).ready(function(){
        $(".editTask3").on("click", (event)=>{
            var userid =  $(event.target).closest('tr').attr("data-user-id");
            var username1 =  $(event.target).closest('tr').attr("data-username1");
            var em =  $(event.target).closest('tr').attr("data-em");
            var birthday =  $(event.target).closest('tr').attr("data-birthday");
            var gender =  $(event.target).closest('tr').attr("data-gender");
                   $('#userid').val(userid);
                   $('#name').val(username1);
                   $('#email').val(em);
                   $('#birthday').val(birthday);
                   $('#gender').val(gender);
            var dataId = $(this).attr("datataskid2");
            document.getElementById('editUserForm').action = "user/edit/"+userid;
            console.log(document.getElementById('editUserForm').action);

        });


    });
</script>
<script>
    $(document).ready(function(){
        $(".getTask").on("click", (event)=>{
            @if(auth()->user()->is_Admin)
             userid = $('#user_id').val();
            @else
             userid =0;
            @endif
            var dateTa = $('#dateT').val();
            // if(dateTa.value ==null)
            document.getElementById('getTa').action = "/task/getTa/"+userid+"/"+dateTa;



        });


    });
</script>

<!-- endinject -->
<!-- Custom js for this page -->
<!-- End custom js for this page -->
</body>
</html>
