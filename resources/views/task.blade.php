<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <title>Login #7</title>
</head>
<body>

            {{$colors = ['red','blue','green','purple']}}
<div class="content" >
    <div class="container">
        <div class="row">

           
            <div class="col-md-12 contents">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <form action="{{route('getUser')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div class="form-group first mt-4" style="padding: 6px">
                                <form action="{{route('getUser')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="user_id">User Name</label>
                                    <select type="submit" class="form-select " style="width: 100%" aria-label="Default select example" style="padding: 6px" name="user_id" id="user_id">
                                        @if (isset($userinf))
                                            @foreach($userinf as $id => $user)
                                                <option value="{{$user['id'] }}">{{$user['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </form>

                            </div> <div class="form-group first mt-4" style="padding: 6px">
                                <label for="name">Task Name</label>
                                <input type="text" name="name" class="form-control">

                            </div>
                            <div class="form-group first mt-4" style="padding: 6px">
                                <label for="description">Description</label>
                                <input type="text" name="description" class="form-control">

                            </div>
                            <label for="user_id">Status</label>
                            <select class="form-select "style="width: 100%"  aria-label="Default select example" style="padding:6px  " name="status" id="status">

                                <option value="pending">pending</option>
                                <option value="inprogress">inprogress</option>
                                <option value="completed">completed</option>
                            </select>



                        </form>
                        <div class="form-group">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button"
                                        data-toggle="dropdown" id="dropdown-toggle">Choisissez le type du produit
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu" name="$userinf" id="$userinf">
                                    @if (isset($$userinf))
                                        @foreach($userinf as $name)
                                            <li><a href="{{$id }}" >{{$name}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
