
<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login',function (){
    return view('login');
});
Route::get('/',function (){
    return view('login');
});


Route::post('/login',[AuthController::class,'login'])->name('login');



Route::middleware('auth')->group(function (){
    Route::get('/register',function (){return view('register');})->middleware('admin');
    Route::get('/logout',[AuthController::class,'logOut']);
    Route::get('/getUser',[AuthController::class,'getUser'])->name('getUser');

    Route::post('/register',[AuthController::class,'register'])->middleware('admin')->name('register');

    Route::prefix('user')->group(function (){
        Route::get('edit/{userId}',[AuthController::class,'edit']);
        Route::get('/delete/{userId}',[AuthController::class,'delete'])->middleware('admin')->name('edtTask');
    });

    Route::prefix('task')->group(function (){
       Route::get('edtTask',function (){return view('edtTask');})->middleware('admin')->name('edtTask');

        Route::post('/create',[TaskController::class,'create'])->middleware('admin')->name('CreateTask');
        Route::get('/edit/{taskId}',[TaskController::class,'edit'])->name('edit');
        Route::get('/dateGet/{taskDate}',[TaskController::class,'dateGet'])->name('dateGet');
        Route::get('/getTa/{userId?}/{dateTa?}',[TaskController::class,'gettask'])->name('gettask');
        Route::get('/deletetask/{taskId}',[TaskController::class,'deletetask'])->middleware('admin')->name('deletetask');
    });

    Route::get('/home/{userId?}',[TaskController::class,'gettask'])->name('home');//بجبلي تاسكات معينة

});
