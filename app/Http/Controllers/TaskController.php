<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\DeleteTaskRequest;
use App\Http\Requests\GetTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Nette\Utils\DateTime;

class TaskController extends Controller
{
    private $task,$user;

    public function __construct(Task $task,User $user)
    {
        $this->task = $task;
        $this->user = $user;
    }

    public function create(CreateTaskRequest $request)
    {
        $data = $request->validated();
        $task = $this->task->create($data);
        $task = $this->task->find(1);
        return redirect()->route('home');

    }

    public function deletetask($taskId)
    {
        $task = $this->task->find($taskId);
        if ($task)
            $task->delete();

        return redirect()->route('home');


    }



    public function gettask( $userId = 0 ,$dateTa =0)
    {
        $getUsertrue=0;

        if($userId==0 && Auth::user()->is_Admin==1){
            $tasks = $this->user->with('task')->get();

        }else if (Auth::user()->is_Admin==1 ){
            $getUsertrue=2;
            $tasks = $this->task->where('user_id',$userId)->get();

        }else if ($userId == 0)
        {
            $getUsertrue = 2;
            $tasks = $this->task->where('user_id', Auth::user()->id)->get();

        }
        $userinf=  $this->user->all(['id', 'name']);
        if ($dateTa!=0)
        {
            $dayAfter = Carbon::parse($dateTa)->modify('+1 day');
            $dateTa=Carbon::parse($dateTa);
            $arry=[];
            $arry=$tasks;
            foreach ($arry as $task)
            foreach($task->task as $t )
            { $tasks = $t->where('created_at', '>=', $dateTa)->where('created_at', '<', $dayAfter)->get();}

        }

//
//        $task = (Auth::user()->is_Admin) ? $this->task->when($userId,function ($task) use ($userId)
//        {
//            $task->where('user_id',$userId);
//        })->get() : $this->task->where('user_id',Auth::user()->id)->get();

        return view('home',compact(['tasks','userinf','getUsertrue']));
    }
    public function edit(UpdateTaskRequest $request,$taskId)
    {
        $data = $request->validated();
        $task = $this->task->where('id', $taskId)->update($data);
        return redirect()->route('home');


        //  return home;
    }
}
