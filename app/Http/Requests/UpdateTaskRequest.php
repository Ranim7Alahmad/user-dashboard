<?php

namespace App\Http\Requests;

use App\Enums\StatusEnum;
use App\Models\Task;
use App\Models\User;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateTaskRequest extends FormRequest
{
    public function authorize()
    {
        $taskId = $this->route('taskId');
        return (Task::where('id', $taskId)->where('user_id', Auth::id())->exists() || Auth::user()->is_Admin == 1);
    }

    public function rules()
    {
        return [
            'status' => ['string', new EnumValue(StatusEnum::class),],
            'name' => ['string'],
            'description' => ['string'],

        ];
    }
}
