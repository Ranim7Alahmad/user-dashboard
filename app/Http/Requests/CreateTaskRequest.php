<?php

namespace App\Http\Requests;

use App\Enums\StatusEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateTaskRequest extends FormRequest
{

    public function rules()
    {
        return [
            'user_id' => ['integer','required', Rule::exists('users', 'id')->whereNull('deleted_at')],
            'description' => ['string', 'required'],
            'name' => ['string', 'required'],
            'status'=> [ 'required','string',new EnumValue(StatusEnum::class),],
        ];
    }
}
