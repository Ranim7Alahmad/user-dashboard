<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetTaskRequest extends FormRequest
{

    public function validationData()
    {
        return array_merge($this->all(), $this->route()->parameters());
    }

    public function rules()
    {
        return [
            'id' => ['integer', 'nullable', Rule::exists('tasks', 'id')->whereNull('deleted_at')],

        ];
    }
}
